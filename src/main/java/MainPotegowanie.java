public class MainPotegowanie {
    public static void main(String[] args) {
        int wynik = obliczPotegeLiczby(2,3, (podstawa, wykladnik)-> (int) Math.pow(podstawa,wykladnik));
        System.out.println(wynik);
    }

    public static int obliczPotegeLiczby(int podstawa, int wykladnik, Potegowanie potegowanie){
        return potegowanie.podniesDoPotegi(podstawa, wykladnik);
    }
}
