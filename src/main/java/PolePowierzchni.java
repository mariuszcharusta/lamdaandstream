@FunctionalInterface
public interface PolePowierzchni {
    double obliczPolePowierzchni(double a, double b);
}
