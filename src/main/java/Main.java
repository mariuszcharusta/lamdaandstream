import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        List<String> produktySpozywcze = Arrays.asList( "Kielbasa", "Chleb", "Mleko", "Maslo", "Chrupki", "Napoj");


        produktySpozywcze.sort((produkt1, produkt2) -> Integer.compare(produkt1.length(), produkt2.length()));


        produktySpozywcze.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return Integer.compare(o1.length(), o2.length());
            }
        });
        System.out.println(produktySpozywcze);
        System.out.println("--------------------STREMY---------------------");
        List<Sklep> obiektyKlasySklepZProduktamiNaLitereM = produktySpozywcze.stream()//tworzymy stream z listy produktow
                .filter(a -> a.startsWith("M")) //tworzymy nowy stream bazujac na poprzednim tylko z produktami na litere m
                .map(produkt -> new Sklep(produkt, new Random().nextDouble()))//zamieniamy pojedynczy produkt na obiekt klasy sklep
                .collect(Collectors.toList());//zbieramy wszystkie obiekty klasy sklep na liste obiektow

        System.out.println(obiektyKlasySklepZProduktamiNaLitereM);

        obiektyKlasySklepZProduktamiNaLitereM.stream()
                .collect(Collectors.toMap(produkt -> produkt.getNazwaTowaru(), produkt -> produkt.getCena()));


    }
}
