@FunctionalInterface
public interface Potegowanie {
    int podniesDoPotegi(int podstawa, int wykladnik);
}
