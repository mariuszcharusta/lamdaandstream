import static java.lang.Math.PI;

public class MainPolePowerzchni {
    public static void main(String[] args) {
        PolePowierzchni poleProstokatu_ = new PolePowierzchni() {
            @Override
            public double obliczPolePowierzchni(double a, double b) {
                return a*b;
            }
        };
        PolePowierzchni poleProstokatu=(a,b)->a*b;
        System.out.println(poleProstokatu.obliczPolePowierzchni(2,2));
        System.out.println(poleProstokatu.obliczPolePowierzchni(2, 3));
        PolePowierzchni poleTrojkata=(a,h)-> (a*h)/2;
        System.out.println(poleTrojkata.obliczPolePowierzchni(10, 2));
        PolePowierzchni poleKola=(r,x)-> Math.PI * Math.pow(r,2); //nie musimy wykorzystywac atrybutu x
        System.out.println(poleKola.obliczPolePowierzchni(3,40));
    }
}
