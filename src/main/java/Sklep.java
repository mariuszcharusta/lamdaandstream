public class Sklep {
    private String nazwaTowaru;
    private Double cena;

    public Sklep(String nazwaTowaru, Double cena) {
        this.nazwaTowaru = nazwaTowaru;
        this.cena = cena;
    }

    @Override
    public String toString() {
        return "Sklep{" +
                "nazwaTowaru='" + nazwaTowaru + '\'' +
                ", cena=" + cena +
                '}';
    }

    public String getNazwaTowaru() {
        return nazwaTowaru;
    }

    public Double getCena() {
        return cena;
    }
}
